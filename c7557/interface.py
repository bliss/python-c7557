"""Python interface to c7557 API."""

from __future__ import absolute_import
from collections import namedtuple
from enum import IntEnum

import numpy

from ._cffi import c7557, ffi

__all__ = ['c7557', 'ffi', 'start_device']

MAX_STRING_LENGTH = 80

# Types

SensorStatus = namedtuple(
    'SensorStatus', 'sensor_type, horizontal_pixels, vertical_pixels, ad_type')


SensorStatus.from_struct = classmethod(lambda cls, struct: cls(
    struct.nSensorType, struct.nHPixel, struct.nVPixel, struct.nADType))


class AmpGain(IntEnum):
    x0_5 = 0
    x1 = 1
    x2 = 2
    x5 = 3
    x10 = 4
    x20 = 5
    x50 = 6
    x100 = 7


# Helpers

def to_bytes(arg):
    if isinstance(arg, bytes):
        return arg
    return arg.encode()


# Initializing c7557

def start_device():
    code = c7557.StartDevice()
    if code == 0xE4:
        raise IOError('Initialization failure')
    if code != 0x00:
        raise IOError('Unknown error code')


def scan_sensor():
    nb_device = c7557.ScanSensor()
    return nb_device


def check_sensor(usb_id):
    status = ffi.new('SENSOR_STATUS *')
    code = c7557.CheckSensor(usb_id, status)
    if code == 0x1001:
        raise IOError('Sensor not connected')
    if code != 0x0001:
        raise IOError('Unknown error code')
    return SensorStatus.from_struct(status[0])


def get_sensor():
    status = ffi.new('SENSOR_STATUS *')
    usb_id = c7557.SelectSensorUnit(status)
    if usb_id in (0xffff, -1):
        raise IOError('No MCD controller connected')
    return usb_id, SensorStatus.from_struct(status[0])


# AMP Gain control

def get_amp_gain(usb_id=None):
    if usb_id is None:
        usb_id, _ = get_sensor()
    amp_gain = c7557.GetAmpGain(usb_id)
    return AmpGain(amp_gain)


def set_amp_gain(amp_gain, usb_id=None):
    if usb_id is None:
        usb_id, _ = get_sensor()
    code = c7557.SetAmpGain(usb_id, int(amp_gain))
    if code == 0x1001:
        raise IOError('Sensor not connected')
    if code != 0x0001:
        raise IOError('Unknown error code')
