/**********************************
	DEFINE (PMA PARAMETER STATUS)
**********************************/

#define	PELTIER_OFF			0		/* Peltier Parameter */
#define	PELTIER_ON			1

#define	INTERNAL_TRIGGER	0		/* Trigger mode & Start mode Parameter */
#define	EXTERNAL_TRIGGER	1
#define	START_TRIGGER		2

#define	POSITIVE_TRIGGER	0		/* Trigger polarity Parameter */
#define	NEGATIVE_TRIGGER	1

#define	SHUTTER_OFF			0		/* Shutter Parameter */
#define	SHUTTER_ON			1

#define	SHUTTER_POSITIVE	0		/* Shutter Polarity Parameter */
#define	SHUTTER_NEGATIVE	1

#define	AMP_GAIN_05			0		/* Amp Gain Parameter */
#define	AMP_GAIN_1			1
#define	AMP_GAIN_2			2
#define	AMP_GAIN_5			3
#define	AMP_GAIN_10			4
#define	AMP_GAIN_20			5
#define	AMP_GAIN_50			6
#define	AMP_GAIN_100		7

#define	AMP_InGaAs			0		/* Amp Range Parameter */
#define	AMP_CCD				1
#define	AMP_InGaAsNew		2		/* 20080414 modify*/
#define	AMP_MOS				3		/* 20080414 modify*/

#define	PELTIER_5V			0		/* Peltier PowerSupply Parameter */
#define	PELTIER_6V			1

#define	AD_12BIT_FAST		0		/* A/D Parameter */
#define	AD_16BIT_SLOW		1
#define	AD_16BIT_FAST		2

#define	MODE_LOAD			0		/* Mode Parameter */
#define	MODE_OPERATE		1

#define	AD_10BIT_TRIGGER_INTERNAL	0		/* Mode Parameter */
#define	AD_10BIT_TRIGGER_EXTERNAL	1

#define	AD_10BIT_TRIGGER_POSITIVE	0		/* Mode Parameter */
#define	AD_10BIT_TRIGGER_NAGATIVE	1

#define	PORT_OPEN			0	/* Port Open / Close */
#define	PORT_CLOSE			1

#define	DATA_7BIT			0	/* RS-232C Data Bit Length */
#define	DATA_8BIT			1

#define	PARITY_BIT_NON		0	/* RS-232C Parity Bit */
#define	PARITY_BIT_EVEN		1
#define	PARITY_BIT_ODD		2

#define	STOP_1BIT			0	/* RS-232C Stop Bit */
#define	STOP_2BIT			1

#define	BAUD_RATE_4800		0	/* RS-232C Baud Rate */
#define	BAUD_RATE_9600		1
#define	BAUD_RATE_19200		2
#define	BAUD_RATE_38400		3


/**********************************
	STRUCT (SENSOR STATUS)
**********************************/

typedef	struct	tag_SENSOR_STATUS
{
	int nSensorType;			/* Line-sensor type */
	int nHPixel;				/* Line-sensor horizontal element type */
	int nVPixel;				/* Line-sensor Vertical element type */
	int nADType;				/* A/D bit type */
} SENSOR_STATUS;


/**********************************
	SET_SUMMING_DATA struct
**********************************/

typedef	struct	tag_SET_SUMMING_DATA
{
	DWORD	dwDataBufferLength;
	LPBYTE	lpbDataBuffer;
} SET_SUMMING_DATA;


/**********************************
	READ_SUMMING_DATA struct
**********************************/

typedef	struct	tag_READ_SUMMING_DATA
{
	DWORD	dwDataBufferLength;
	LPBYTE	lpbDataBuffer;
} READ_SUMMING_DATA;


/**********************************
	PORT_CONTROL struct
**********************************/

typedef	struct	tag_PORT_CONTROL
{
	BYTE	bTransferParameter;
	BYTE	bBaudRate;
	BYTE	bPortControl;
} PORT_CONTROL;


/**********************************
	SENSOR32.DLL FUNCTION
**********************************/

WORD ScanSensor();
WORD CheckSensor(WORD wDeviceID, SENSOR_STATUS* pStatus);
WORD SelectSensorUnit(SENSOR_STATUS* pStatus);
WORD SetPeltierControl(WORD wDeviceID, WORD wPeltierControl);
WORD GetPeltierControl(WORD wDeviceID);
WORD SetAmpGain(WORD wDeviceID, WORD wAmpGain);
WORD GetAmpGain(WORD wDeviceID);
WORD SetExposureTime(WORD wDeviceID, WORD wExposureTime);
WORD GetExposureTime(WORD wDeviceID);
WORD SetPixelClockTime(WORD wDeviceID, WORD wPixelClockTime);
WORD GetPixelClockTime(WORD wDeviceID);
WORD GetTemperature(WORD wDeviceID);
WORD GetProfile(WORD wDeviceID, WORD wNumberOfCycle, LPWORD pData);
WORD Get10BitADData(WORD wDeviceID);
WORD SetTriggerMode(WORD wDeviceID, WORD wTriggerMode);
WORD GetTriggerMode(WORD wDeviceID);
WORD SetTriggerPolarity(WORD wDeviceID, WORD wTriggerPolarity);
WORD GetTriggerPolarity(WORD wDeviceID);
WORD SetShutterControl(WORD wDeviceID, WORD wShutterControl);
WORD GetShutterControl(WORD wDeviceID);
WORD SetShutterDelayTime(WORD wDeviceID, WORD wShutterDelayTime);
WORD GetShutterDelayTime(WORD wDeviceID);
WORD SetShutterDurationTime(WORD wDeviceID, WORD wShutterDurationTime);
WORD GetShutterDurationTime(WORD wDeviceID);
WORD SetShutterPolarity(WORD wDeviceID, WORD wShutterPolarity);
WORD GetShutterPolarity(WORD wDeviceID);
WORD SetDelayTime(WORD wDeviceID, WORD wDelayTime);
WORD GetDelayTime(WORD wDeviceID);
WORD SetSensorType(WORD wDeviceID, WORD wSensorType);
WORD GetSensorType(WORD wDeviceID);
WORD SetPeltierPower(WORD wDeviceID, WORD wPeltierPower);
WORD GetPeltierPower(WORD wDeviceID);
WORD SetADType(WORD wDeviceID, WORD wADType);
WORD GetADType(WORD wDeviceID);
WORD SetModeSelect(WORD wDeviceID, WORD wModeSelect);
WORD GetModeSelect(WORD wDeviceID);
WORD SetSensorHch(WORD wDeviceID, WORD wSensorHch);
WORD GetSensorHch(WORD wDeviceID);
WORD SetSensorVch(WORD wDeviceID, WORD wSensorVch);
WORD GetSensorVch(WORD wDeviceID);
WORD SetSummingHch(WORD wDeviceID, WORD wSummingHch);
WORD GetSummingHch(WORD wDeviceID);
WORD SetSummingVch(WORD wDeviceID, WORD wSummingVch);
WORD GetSummingVch(WORD wDeviceID);
WORD SetDA1(WORD wDeviceID, WORD wDA1);
WORD GetDA1(WORD wDeviceID);
WORD SetDA2(WORD wDeviceID, WORD wDA2);
WORD GetDA2(WORD wDeviceID);
WORD Set10BitADTriggerMode(WORD wDeviceID, WORD w10BitADTriggerMode);
WORD Get10BitADTriggerMode(WORD wDeviceID);
WORD Set10BitADTriggerPolarity(WORD wDeviceID, WORD w10BitADTriggerPolarity);
WORD Get10BitADTriggerPolarity(WORD wDeviceID);
WORD SendSummingData(WORD wDeviceID, SET_SUMMING_DATA* pSummingData);
WORD ReadSummingData(WORD wDeviceID, READ_SUMMING_DATA* pSummingData);
WORD StartDevice();
WORD EndDevice();

typedef	WORD (*ProcScanSensor)();
typedef	WORD (*ProcCheckSensor)(WORD, SENSOR_STATUS*);
typedef	WORD (*ProcSelectSensorUnit)(SENSOR_STATUS*);
typedef	WORD (*ProcSetPeltierControl)(WORD, WORD);
typedef	WORD (*ProcGetPeltierControl)(WORD);
typedef	WORD (*ProcSetAmpGain)(WORD, WORD);
typedef	WORD (*ProcGetAmpGain)(WORD);
typedef	WORD (*ProcSetExposureTime)(WORD, WORD);
typedef	WORD (*ProcGetExposureTime)(WORD);
typedef	WORD (*ProcSetPixelClockTime)(WORD, WORD);
typedef	WORD (*ProcGetPixelClockTime)(WORD);
typedef	WORD (*ProcGetTemperature)(WORD);
typedef	WORD (*ProcGetProfile)(WORD, WORD, LPWORD);
typedef	WORD (*ProcSetTriggerMode)(WORD, WORD);
typedef	WORD (*ProcGetTriggerMode)(WORD);
typedef	WORD (*ProcSetTriggerPolarity)(WORD, WORD);
typedef	WORD (*ProcGetTriggerPolarity)(WORD);
typedef	WORD (*ProcSetShutterControl)(WORD, WORD);
typedef	WORD (*ProcGetShutterControl)(WORD);
typedef	WORD (*ProcSetShutterDelayTime)(WORD, WORD);
typedef	WORD (*ProcGetShutterDelayTime)(WORD);
typedef	WORD (*ProcSetShutterDurationTime)(WORD, WORD);
typedef	WORD (*ProcGetShutterDurationTime)(WORD);
typedef	WORD (*ProcSetShutterPolarity)(WORD, WORD);
typedef	WORD (*ProcGetShutterPolarity)(WORD);
typedef	WORD (*ProcSetDelayTime)(WORD, WORD);
typedef	WORD (*ProcGetDelayTime)(WORD);
typedef	WORD (*ProcSetSensorType)(WORD, WORD);
typedef	WORD (*ProcGetSensorType)(WORD);
typedef	WORD (*ProcSetPeltierPower)(WORD, WORD);
typedef	WORD (*ProcGetPeltierPower)(WORD);
typedef	WORD (*ProcSetADType)(WORD, WORD);
typedef	WORD (*ProcGetADType)(WORD);
typedef	WORD (*ProcSetModeSelect)(WORD, WORD);
typedef	WORD (*ProcGetModeSelect)(WORD);
typedef	WORD (*ProcSetSensorHch)(WORD, WORD);
typedef	WORD (*ProcGetSensorHch)(WORD);
typedef	WORD (*ProcSetSensorVch)(WORD, WORD);
typedef	WORD (*ProcGetSensorVch)(WORD);
typedef	WORD (*ProcSetSummingHch)(WORD, WORD);
typedef	WORD (*ProcGetSummingHch)(WORD);
typedef	WORD (*ProcSetSummingVch)(WORD, WORD);
typedef	WORD (*ProcGetSummingVch)(WORD);
typedef	WORD (*ProcSetDA1)(WORD, WORD);
typedef	WORD (*ProcGetDA1)(WORD);
typedef	WORD (*ProcSetDA2)(WORD, WORD);
typedef	WORD (*ProcGetDA2)(WORD);
typedef	WORD (*ProcSet10BitADTriggerMode)(WORD, WORD);
typedef	WORD (*ProcGet10BitADTriggerMode)(WORD);
typedef	WORD (*ProcSet10BitADTriggerPolarity)(WORD, WORD);
typedef	WORD (*ProcGet10BitADTriggerPolarity)(WORD);
typedef	WORD (*ProcGet10BitADData)(WORD);
typedef	WORD (*ProcSendSummingData)(WORD, SET_SUMMING_DATA*);
typedef	WORD (*ProcReadSummingData)(WORD, READ_SUMMING_DATA*);
