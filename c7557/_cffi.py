"""CFFI binding."""

import os
import cffi

ffi = cffi.FFI()

# Windows types

ffi.cdef("""
typedef unsigned long DWORD;
typedef unsigned short WORD;
typedef unsigned char BYTE;
typedef BYTE *LPBYTE;
typedef WORD *LPWORD;
""")

# C7557 header

package_dir = os.path.dirname(os.path.join(__file__))
headerfile = os.path.join(package_dir, 'header.h')
with open(headerfile) as f:
    ffi.cdef(f.read())

c7557 = ffi.dlopen("McdMain.dll")
