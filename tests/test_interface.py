import pytest
import numpy
import mock


@pytest.fixture
def interface():
    with mock.patch('cffi.FFI.dlopen') as dlopen:
        from c7557 import interface
        interface.c7557 = dlopen.return_value
        yield interface


# Initializing c7557

def test_start_device(interface):
    m = interface.c7557.StartDevice
    # No error
    m.return_value = 0
    assert interface.start_device() is None
    m.assert_called_once_with()
    # Initialization failure
    m.return_value = 0xE4
    with pytest.raises(IOError) as context:
        interface.start_device()
    assert 'Initialization failure' in str(context.value)
    # Unknown error code
    m.return_value = 0x01
    with pytest.raises(IOError) as context:
        interface.start_device()
    assert 'Unknown error code' in str(context.value)


def test_scan_sensor(interface):
    m = interface.c7557.ScanSensor
    m.return_value = 3
    assert interface.scan_sensor() == 3
    m.assert_called_once_with()


def test_check_sensor(interface):
    m = interface.c7557.CheckSensor

    def side_effect(a, b):
        b[0].nSensorType = 1
        b[0].nHPixel = 2
        b[0].nVPixel = 3
        b[0].nADType = 4
        return 1

    m.side_effect = side_effect
    assert interface.check_sensor(1) == interface.SensorStatus(1, 2, 3, 4)
    arg = m.call_args[0][1]
    m.assert_called_once_with(1, arg)
    m.reset_mock()

    m.side_effect = None
    m.return_value = 0x1001
    with pytest.raises(IOError) as context:
        interface.check_sensor(1)
    assert "Sensor not connected" in str(context.value)
    arg = m.call_args[0][1]
    m.assert_called_once_with(1, arg)
    m.reset_mock()

    m.side_effect = None
    m.return_value = 0x02
    with pytest.raises(IOError) as context:
        interface.check_sensor(1)
    assert "Unknown error code" in str(context.value)
    arg = m.call_args[0][1]
    m.assert_called_once_with(1, arg)
    m.reset_mock()


def test_get_sensor(interface):
    m = interface.c7557.SelectSensorUnit

    def side_effect(a):
        a[0].nSensorType = 1
        a[0].nHPixel = 2
        a[0].nVPixel = 3
        a[0].nADType = 4
        return 3

    m.side_effect = side_effect
    assert interface.get_sensor() == (3, interface.SensorStatus(1, 2, 3, 4))
    arg = m.call_args[0][0]
    m.assert_called_once_with(arg)
    m.reset_mock()

    m.side_effect = None
    m.return_value = 0xffff
    with pytest.raises(IOError) as context:
        interface.get_sensor()
    assert "No MCD controller connected" in str(context.value)
    arg = m.call_args[0][0]
    m.assert_called_once_with(arg)
    m.reset_mock()


# AMP Gain control

def test_get_amp_gain(interface):
    m1 = interface.c7557.SelectSensorUnit
    m1.return_value = 3
    m2 = interface.c7557.GetAmpGain
    m2.return_value = 4

    assert interface.get_amp_gain() == interface.AmpGain.x10
    m2.assert_called_once_with(3)


def test_set_amp_gain(interface):
    m1 = interface.c7557.SelectSensorUnit
    m1.return_value = 3
    m2 = interface.c7557.SetAmpGain
    m2.return_value = 0x0001

    assert interface.set_amp_gain(interface.AmpGain.x20) is None
    m2.assert_called_once_with(3, 5)
    m2.reset_mock()

    m2.return_value = 0x1001
    with pytest.raises(IOError) as context:
        interface.set_amp_gain(interface.AmpGain.x50)
    assert "Sensor not connected" in str(context.value)
    m2.assert_called_once_with(3, 6)
    m2.reset_mock()

    m2.return_value = 0x0002
    with pytest.raises(IOError) as context:
        interface.set_amp_gain(interface.AmpGain.x100)
    assert "Unknown error code" in str(context.value)
    m2.assert_called_once_with(3, 7)
    m2.reset_mock()
