import sys
from setuptools import setup

TESTING = any(x in sys.argv for x in ['test', 'pytest'])

with open("README.md") as f:
    long_description = f.read()

setup(
    name='c7557',
    version='0.1.0.dev0',

    packages=['c7557'],
    install_requires=['cffi', 'numpy', 'enum34'],
    setup_requires=['pytest-runner', 'pytest'] if TESTING else [],
    tests_require=['pytest-cov', 'mock'],

    package_data={'c7557': ['header.h']},

    description="Python binding for the Hamamatsu c7557 library",
    long_description=long_description,

    license="GPLv3",
    author="Vincent Michel")
